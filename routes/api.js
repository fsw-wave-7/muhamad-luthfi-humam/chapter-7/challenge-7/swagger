const api = require("express").Router()
const AuthController = require('../controllers/api/authController')
const UserController = require('../controllers/api/userController')
const authController = new AuthController();
const userController = new UserController();
const bodyParser = require('body-parser')
const RestrictApi = require('../middlewares/restrict-api')
api.use(bodyParser.json())

api.post('/register', authController.register)
api.post('/login', authController.login)

api.use(RestrictApi)

api.post('/logout', authController.logout)
api.delete('/:id', userController.deleteUser)

api.get('/', userController.getUser)
api.get('/:id', userController.getDetailUser)
api.post('/insertbiodata', userController.insertBiodata)
api.post('/insertscore', userController.insertScore)
api.post('/createRoom', userController.createRoom);
api.post('/fight/:room', userController.fightRoom);

module.exports = api